from api.models import Car, Rate
from django.db.models import Avg
from rest_framework import serializers


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ("id", "model", "make")


class CarWithAverageSerializer(serializers.ModelSerializer):
    avg_rating = serializers.SerializerMethodField()

    def get_avg_rating(self, obj):
        return Rate.objects.filter(car=obj.id).aggregate(Avg('rating'))["rating__avg"]

    class Meta:
        model = Car
        fields = ("id", "model", "make", "avg_rating")


class CarPopularSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ("id", "model", "make", "rates_number")
