from api.models import Car, Rate
from django.test import TestCase


class CarTestCase(TestCase):
    def setUp(self):
        self.bmw = Car.objects.create(model="E36", make="BMW")
        self.fiat = Car.objects.create(model="Multipla", make="Fiat")

    def test_car_model_creation(self):
        self.assertEqual(Rate.objects.count(), 0)

        bmw_rate = Rate.objects.create(car=self.bmw, rating=2)
        fiat_rate = Rate.objects.create(car=self.fiat, rating=4)

        self.assertEqual(Rate.objects.count(), 2)

        self.assertTrue(isinstance(bmw_rate, Rate))
        self.assertEqual(bmw_rate.car, self.bmw)
        self.assertEqual(bmw_rate.__str__(), "E36 - BMW - 2")

        self.assertTrue(isinstance(fiat_rate, Rate))
        self.assertEqual(fiat_rate.car, self.fiat)
        self.assertEqual(fiat_rate.__str__(), "Multipla - Fiat - 4")