from api.models import Car
from django.test import TestCase


class CarTestCase(TestCase):

    def test_car_model_creation(self):
        self.assertEqual(Car.objects.count(), 0)

        Car.objects.create(model="E36", make="BMW")
        Car.objects.create(model="Multipla", make="Fiat")

        self.assertEqual(Car.objects.count(), 2)

        bmw = Car.objects.get(model="E36")
        fiat = Car.objects.get(model="Multipla")

        self.assertTrue(isinstance(bmw, Car))
        self.assertEqual(bmw.make, "BMW")
        self.assertEqual(bmw.__str__(), "E36 - BMW")
        self.assertEqual(bmw.rates_number, 0)

        self.assertTrue(isinstance(fiat, Car))
        self.assertEqual(fiat.make, 'Fiat')
        self.assertEqual(fiat.__str__(), "Multipla - Fiat")
        self.assertEqual(fiat.rates_number, 0)
