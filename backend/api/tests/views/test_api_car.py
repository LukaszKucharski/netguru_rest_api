from api.models import Car, Rate
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


class CarApiTests(TestCase):

    def test_create_car(self):
        self.assertEqual(Car.objects.count(), 0)

        url = reverse("cars-list")
        client = APIClient()
        data = {'model': 'G 650', 'make': 'BMW'}
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Car.objects.count(), 1)

        data = {'model': 'G', 'make': 'BMW'}
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(Car.objects.count(), 1)

        data = {'model': 'G', 'make': 'Netguru'}
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(Car.objects.count(), 1)

    def test_delete_car(self):
        Car.objects.create(model="E36", make="BMW")

        car_number = Car.objects.count()
        car = Car.objects.last().id

        url = f"/api/v1/cars/{car}/"
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(car_number - 1, Car.objects.count())

    def test_get_list_cars(self):
        car = Car.objects.create(model="E36", make="BMW")
        first_rate = Rate.objects.create(rating=5, car=car)
        second_rate = Rate.objects.create(rating=1, car=car)
        second_car = Car.objects.create(model="Multipla", make="Fiat")

        url = reverse("cars-list")
        client = APIClient()
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data[0]["avg_rating"], 3)
        self.assertEqual(response.data[1]["avg_rating"], None)
