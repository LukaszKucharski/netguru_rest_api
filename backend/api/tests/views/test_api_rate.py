from api.models import Car, Rate
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


class RateApiTests(TestCase):
    def setUp(self):
        self.car = Car.objects.create(model="E36", make="BMW")

    def test_create_rate(self):
        self.assertEqual(Rate.objects.count(), 0)
        self.assertEqual(self.car.rates_number, 0)

        url = reverse("rates-list")
        client = APIClient()
        data = {'car': self.car.id, 'rating': '1'}
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Rate.objects.count(), 1)

        updated_car = Car.objects.get(id=self.car.id)
        self.assertEqual(updated_car.rates_number, 1)
