from api.models import Car, Rate
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


class PopularCarApiTests(TestCase):

    def setUp(self):
        first_car = Car.objects.create(model="E36", make="BMW", rates_number="13")
        second_car = Car.objects.create(model="Multipla", make="Fiat", rates_number="230")
        third_car = Car.objects.create(model="Astra", make="Opel")

    def test_get_list_popular_cars(self):
        url = reverse("popular-list")
        client = APIClient()
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data[0]["rates_number"], 230)
        self.assertEqual(response.data[0]["model"], "Multipla")

        self.assertEqual(response.data[1]["rates_number"], 13)
        self.assertEqual(response.data[1]["model"], "E36")

        self.assertEqual(response.data[2]["rates_number"], 0)
        self.assertEqual(response.data[2]["model"], "Astra")
