from rest_framework import routers

from .views import CarViewSet, RateViewSet, PopularCarViewSet

router = routers.SimpleRouter()
router.register(r'cars', CarViewSet, basename='cars')
router.register(r'rate', RateViewSet, basename='rates')
router.register(r'popular', PopularCarViewSet, basename='popular')
urlpatterns = router.urls
