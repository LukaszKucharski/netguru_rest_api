from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from . import Car


class Rate(models.Model):
    """
        Stores information about rates.
    """
    rating = models.IntegerField(validators=[MaxValueValidator(5), MinValueValidator(1)])
    car = models.ForeignKey(Car, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.car} - {self.rating}"
