from django.db import models


class Car(models.Model):
    """
        Stores information about cars.
    """
    make = models.CharField(max_length=100)
    model = models.CharField(max_length=100, unique=True)
    rates_number = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.model} - {self.make}"
