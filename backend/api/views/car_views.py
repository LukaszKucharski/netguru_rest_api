import requests
from api.models import Car
from api.serializers import CarSerializer, CarWithAverageSerializer
from django.http import Http404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response


class CarViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for adding, listing or deleting cars.
    """

    def create(self, request):
        serializer = CarSerializer(data=request.data)
        request = request.data
        url = f'https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/{request["make"]}?format=json'
        response = requests.get(url).json()

        if response['Count'] > 0:
            models = [x['Model_Name'] for x in response['Results']]
            if not request['model'] in models:
                raise Http404
        else:
            raise Http404

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        car = self.get_object(pk)
        car.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def list(self, request):
        queryset = Car.objects.all()
        serializer = CarWithAverageSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_object(self, pk):
        try:
            return Car.objects.get(pk=pk)
        except Car.DoesNotExist:
            raise Http404
