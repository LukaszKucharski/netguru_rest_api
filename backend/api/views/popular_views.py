from api.models import Car
from api.serializers import CarPopularSerializer
from rest_framework import viewsets
from rest_framework.response import Response


class PopularCarViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing cars ordered by popularity.
    """

    def list(self, request):
        queryset = Car.objects.all().order_by("-rates_number")
        serializer = CarPopularSerializer(queryset, many=True)
        return Response(serializer.data)
