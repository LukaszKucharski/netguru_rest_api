from api.models import Car
from api.serializers import RateSerializer
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response


class RateViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for adding rates.
    """

    def increment_rates_number(self, pk):
        car = Car.objects.get(id=pk)
        car.rates_number += 1
        car.save()

    def create(self, request):
        serializer = RateSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            self.increment_rates_number(pk=request.data['car'])
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
