#!/bin/bash
sleep 10
echo ''
echo '--------------------------'
echo 'Database migration'
echo '--------------------------'
echo ''
python manage.py makemigrations
python manage.py migrate

echo ''
echo '--------------------------'
echo 'Create admin account'
echo '--------------------------'
echo ''
python manage.py ensure_adminuser --username=admin --email=admin@example.com --password=pass
echo ''
echo '--------------------------'
echo 'Run server'
echo '--------------------------'
echo ''
python manage.py runserver 0.0.0.0:80
